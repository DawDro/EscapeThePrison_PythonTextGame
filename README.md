Project was created to pass exam in Z2J Python training path. 

Project is a simple text game to practice loops, classes, functions and other topics from previous lessons.

Gameplay can be seen in following link: https://youtu.be/-yvrb7Jq8y0
