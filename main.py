# main.py

from random import randint
from classes import Player

## Welcome and chapter 1 messages
welcome_prison_msg = "Welcome in 'Escape The Prison' game!\nTo move, look around or take action please use proper commands that will be visible on your screen."
chapter1_msg = "You woke up in a prison cell, barerly remember what happend. It looks like something happened in the prison, there is much smoke around."
action_msg1 = "a) Look around\nb) Call the guard\n"
look_around_msg = "You get up and look around. You are alone in cell. There are bars in front of you that are damaged, the guard lays by the bars on the ground, he is unconscious. What would you do?"
call_the_guard_msg = "Noone answers, the only thing you hear are screams and noises of fire."
action_msg2 = "a) Search the guard's body\nb) Run from your cell to find prison's exit\n"

## Chapter 2 messages 
run_items_msg = "You found gun and access card and started running to the prison exit."
run_no_items_msg = "You started running to the prison exit."
run_into_guard = "While running to exit you see the guard in front of you."
action_msg3 = "a) Try to run past the guard\nb) Shoot him with the gun\nc) Show your equipement\n"

## Chapter 3 messages 
knock_out_by_guard_msg = "While trying to get past the guard, he knocked you down and you fainted. The guard took you to another cell. Game Over!"
no_gun = "You do not have a gun. It seems that trying to run past the guard is your only option."
passed_guard = "You managed to pass the guard and continue running towards the exit until you encounter a gate. You see that there is a place to put the access card to open the gate."
killed_guard_msg = "You managed to kill the guard and continue running towards the exit until you encounter a gate. You see that there is a place to put the access card to open the gate."
action_msg4 = "a) Run to another exit\nb) Use access card\nc) Show your equipement\n"

## Chapter 4 messages 
you_won = "It seems that card worked. The doors are opening and you see the freedom behind the door. Congratulations! You escaped the prison!"
no_card = "You do not have card. It seems that trying to run to another exit is your only option."

incorrect_action = "Incorrect action. Please choose A or B to continue."

player = Player()

print(welcome_prison_msg)

def chapter1():
    print(chapter1_msg)
    while True:
        action1 = input(action_msg1).upper()
        if action1 == "A":
            print(look_around_msg)
            return chapter2()
        elif action1 == "B":
            print(call_the_guard_msg)
        else:
            print(incorrect_action)

def chapter2():
    while True:
        action2 = input(action_msg2).upper()
        if action2 == "A":
            print(run_items_msg)
            player.take_items("gun")
            player.take_items("access card")
            return chapter3()
        elif action2 == "B":
            print(run_no_items_msg)
            return chapter3()
        elif action2 == "C":
            player.show_items()
            return chapter3()
        else:
            print(incorrect_action)

def chapter3():
    print(run_into_guard)
    while True:
        action3 = input(action_msg3).upper()
        if action3 == "A":
            if_passed = randint(0,1)
            if if_passed == 0:
                print(knock_out_by_guard_msg)
                return game_over()
            else:
                print(passed_guard)
                return chapter4()                  
        elif action3 == "B":
            if "gun" in player.hands:
                print(killed_guard_msg)
                return chapter4()
            else:
                print(no_gun)
        elif action3 == "C":
            player.show_items()
        else:
            print(incorrect_action)

def chapter4():
    while True:
        action4 = input(action_msg4).upper()
        if action4 == "A":
            return chapter3()
        elif action4 == "B":
            if "access card" in player.hands:
                print(you_won)
                return game_over()
            else:
                print(no_card)
        elif action4 == "C":
            player.show_items()
        else:
            print(incorrect_action)

def game_over():
    while True:
        go_action = input("Would you like to play again?\na) Play again!\nb) Exit the game.").upper()
        if go_action == "A":
            player.hands = []
            return chapter1()
        elif go_action == "B":
            break
        else:
            print(incorrect_action)


chapter1()